#!/bin/bash

PSQL="psql -U postgres"
PSQLT="psql -U tipset -d postgres"

if [ -z "$TIPSET_PASSWORD" ]; then
    echo "INFO: Using default password. Use \"-e TIPSET_PASSWORD=password\"" \
	"to set it in \"docker run\""
fi

create_user_if_not_exist() {
    DEFAULT_PASSWORD="tipset"
    PASSWORD=${TIPSET_PASSWORD:-$DEFAULT_PASSWORD}
    $PSQL -tc "SELECT 1 FROM pg_user WHERE usename = 'tipset'" | \
        grep -q 1 || $PSQL -c "CREATE USER tipset CREATEDB PASSWORD '$PASSWORD'"
    return 0
}

drop_database_if_exist() {
    local db=$1
    $PSQL -tc "SELECT 1 FROM pg_database WHERE datname = 'tipset_${db}'" | \
        grep -q 1 && $PSQLT -c "DROP DATABASE tipset_${db}"
    return 0
}

create_database_unless_exist() {
    local db=$1
    $PSQL -tc "SELECT 1 FROM pg_database WHERE datname = 'tipset_${db}'" | \
        grep -q 1 || $PSQLT -c "CREATE DATABASE tipset_${db} WITH OWNER tipset"
    return 0
}

create_user_if_not_exist
drop_database_if_exist "test"
drop_database_if_exist "dev"
create_database_unless_exist "test"
create_database_unless_exist "dev"
create_database_unless_exist "prod"

# Enable password auth for remote hosts
sed -i 's/host all all all trust/host all all all password/' \
    /var/lib/postgresql/data/pg_hba.conf
